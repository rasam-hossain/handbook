---

title: "CSM/CSE Webinar & Hands-On Labs Calendar"
---
# On this page



View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---
# Upcoming Events

We’d like to invite you to our free upcoming webinars and labs in the month of March 2024.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## April 2024

### AMER Time Zone Webinars & Labs

#### Intro to GitLab
##### April 3rd, 2024 at 9:00-10:00AM PT / 12:00-1:00PM ET

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_VibxsqwhSTq3Z_0KPPlVqg#/registration)

#### Intro to CI/CD
##### April 9th, 2024 at 9:00-10:00AM PT / 12:00-1:00PM ET

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_gE1n9NBhRp2AI7ogdCNBhw#/registration)

#### Hands-On GitLab CI Lab 
##### April 10th, 2024 at 9:00-11:00AM PT / 12:00-2:00PM ET

Join us for a hands-on GitLab CI lab and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_5JolkHESQCOPVIWxYEQjgg#/registration)

### Jira to GitLab: Helping you transition to planning with GitLab
##### April 16th, 2024 at 9:00-10:00AM PT / 12:00-1:00PM ET

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Y7sn8gWvQeixZ2hI3ZYVbg#/registration)

#### Hands-on Lab: Security and Compliance in GitLab
##### April 17th, 2024 at 9:00-11:00AM PT / 12:00-2:00PM ET

In this lab we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_z3yMsYLQTpqiDzAC0RcbsQ#/registration)

#### Hands-On Advanced GitLab CI Lab 
##### April 24th, 2024 at 9:00-11:00AM PT / 12:00-2:00PM ET

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_D_pFLbsvRS-bLD0p7ut24g#/registration)

#### AI in DevSecOps - GitLab Webinar
##### April 30th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Join us for the AI in DevSecOps webinar where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-kIPWEcnTjOzVrKplN126g#/registration)

### APJ Time Zone Webinars & Labs

#### GitLab Runner Fundamentals and What You Need to Know
##### April 9th, 2024 at 10:30-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 3:00-4:00PM Sydney  

Runners are a requirement for operating GitLab CI/CD. Learn about Runner architecture, options for deployments, supported operating systems, and optimizing them for most efficient usage. We will be discussing best practices, decisions that you need to make before deploying at scale, and options you have for monitoring fleets of runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_WEeZbf8XSFCiBqwonQ_XgA#/registration)


### Jira to GitLab: Helping you transition to planning with GitLab
##### April 11th, 2024 at 10:30-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 3:00-4:00PM Sydney  

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BQP74WAFTtiVOPPzdIup8w#/registration)


### EMEA Time Zone Webinars & Labs

#### Intro to GitLab
##### April 3rd, 2024 at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_6_m8HUMTT5a0JkOeM5FJ8Q#/registration)

#### Intro to CI/CD
##### April 9th, 2024 at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8h1UkhD-ThaRsXHLw79YxQ#/registration)

#### Hands-On GitLab CI Lab 
##### April 10th, 2024 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

Join us for a hands-on GitLab CI lab and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_tvijqCrhQSyebWuVslTKcQ#/registration)

### Jira to GitLab: Helping you transition to planning with GitLab
##### April 16th, 2024 at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_FtT-Df2ERHG6mQKs16EzeQ#/registration)

#### Hands-on Lab: Security and Compliance in GitLab
##### April 17th, 2024 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

In this lab we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-VO1nxj9Sl-rX7pYaiZPng#/registration)

#### Hands-On Advanced GitLab CI Lab 
##### April 24th, 2024 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_OQNV9scJSIaelQkXl5mbnw#/registration)

#### AI in DevSecOps - GitLab Webinar
##### April 30th, 2024 at at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Join us for the AI in DevSecOps webinar where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__PRkLukaTyqA1uwpoe9OwA#/registration)



Check back later for more webinars & labs! 
